<?php
/**
 * @file
 * Callbacks for services module resource hooks.
 */

/**
 * Callback for rating an entity.
 *
 * @param $id
 *   Int (required). The entity ID. Defaults to 'node'.
 * @param $rating
 *   Int (required). A number rating from 1-5. The number is multiplied by 20 before being processed.
 * @param $entity_type
 *   Int (optional). An offset integer for paging.
 * @param $tag
 *   String (optional). Defaults to 'vote'
 * @param $uid
 *   Int (optional). User ID.
 * @param $skip_validation
 *   Bool (optional). Defaults to FALSE.
 *
 * @return
 *  Array. The fivestar rating return.
 */
 
function _services_fivestar_rate($id, $rating, $entity_type = NULL, $tag = NULL, $uid = NULL, $skip_validation = FALSE) {
  global $user;
  
  $rating = $rating * 20;
  $entity_type = empty($entity_type) ? 'node' : $entity_type;
  $tag = empty($tag) ? 'vote' : $tag;
  $uid = isset($uid) ? $uid : $user->uid;
  // Bail out if the user's trying to vote on an invalid object.
  if (!$skip_validation && !fivestar_validate_target($entity_type, $id, $tag, $uid)) {
    return array();
  }
  // Sanity-check the incoming values.
  if (is_numeric($id) && is_numeric($rating)) {
    if ($rating > 100) {
      $rating = 100;
    }


    // Get the user's current vote.
    $criteria = array('entity_type' => $entity_type, 'entity_id' => $id, 'tag' => $tag, 'uid' => $uid);
    // Get the unique identifier for the user (IP Address if anonymous).
    $user_criteria = votingapi_current_user_identifier();
    $user_votes = votingapi_select_votes($criteria + $user_criteria);

    if ($rating == 0) {
      votingapi_delete_votes($user_votes);
    }
    else {
      $votes = $criteria += array('value' => $rating);
      votingapi_set_votes($votes);
    }

    // Moving the calculationg after saving/deleting the vote but before getting the votes.
    votingapi_recalculate_results($entity_type, $id);    
    return fivestar_get_votes($entity_type, $id, $tag, $uid);
  }

}

/**
 * Callback for retrieving an entity's rating.
 *
 * @param $id
 *   Int (required). The entity ID. Defaults to 'node'.
 * @param $entity_type
 *   Int (optional). An offset integer for paging.
 * @param $tag
 *   String (optional). Defaults to 'vote'
 * @param $uid
 *   Int (optional). User ID.
 *
 * @return
 *  Array. The fivestar return.
 */

function _services_fivestar_retrieve($id, $entity_type = NULL, $tag = NULL, $uid = NULL) {
  global $user;

  $entity_type = empty($entity_type) ? 'node' : $entity_type;
  $tag = empty($tag) ? 'vote' : $tag;
  
  if (!isset($uid)) {
    $uid = $user->uid;
  }

  $criteria = array(
    'entity_type' => $entity_type,
    'entity_id' => $id,
    'value_type' => 'percent',
    'tag' => $tag,
  );

  $ratings = array(
    'average' => array(),
    'count' => array(),
    'user' => array(),
  );

  $results = votingapi_select_results($criteria);
  foreach ($results as $result) {
    if ($result['function'] == 'average') {
      $ratings['average'] = $result;
    }
    if ($result['function'] == 'count') {
      $ratings['count'] = $result;
    }
  }
  
  if ($uid) {
    $user_vote = votingapi_select_votes($criteria += array('uid' => $uid));
    if ($user_vote) {
      $ratings['user'] = $user_vote[0];
      $ratings['user']['function'] = 'user';
    }
  }
  else {
    // If the user is anonymous, we never bother loading their existing votes.
    // Not only would it be hit-or-miss, it would break page caching. Safer to always
    // show the 'fresh' version to anon users.
    $ratings['user'] = array('value' => 0);
  }
  
  return $ratings;
}